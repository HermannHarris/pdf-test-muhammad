import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

import WeatherList from './components/weatherList';
import { Divider } from 'semantic-ui-react'

class App extends Component {
  render() {
    return (
      <Divider className="App">
        <WeatherList />
      </Divider>
    );
  }
}

export default App;
