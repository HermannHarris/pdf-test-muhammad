import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, browserHistory } from 'react-router';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import './index.css';
import registerServiceWorker from './registerServiceWorker';

import reducers from './reducers/index';

import App from './App';

//REDUX principle: Only a single STORE per application can hold one STATE which should be immutable 
const store = createStore(reducers, applyMiddleware(thunk, logger));

ReactDOM.render(
  <Provider store={ store }>
    <Router history={ browserHistory }>
      <Route path='/' component={ App } />
    </Router>
  </Provider>, document.getElementById('root')
);
registerServiceWorker();
