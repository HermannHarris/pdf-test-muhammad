import { FETCH_WEATHER } from '../actions/index';

const weatherReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_WEATHER:
      return [ action.payload, ...state ]; //REDUX principle: return immutable state 
      break;
    default:
      return state;
  }
};

export default weatherReducer;
