import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import pdfConverter from 'jspdf';
import { Button, Table, Header, Divider } from 'semantic-ui-react';
import * as weatherActions from '../actions/';

class WeatherList extends React.Component{

  /*
  * @params: city, countryCode
  * @function: fetchWeather(@param1, @param2)
  * when this smart component (container) is rendered, it will dispatch fetchWeather(city, countryCode) action
  * which will send a FETCH REQUEST to Weather API named 'OPENWEATHER API' , Link: 'https://openweathermap.org/api'
  * API is return forecast of few days i.e. approx 5 days and AVERAGE Temperature, Pressure, Humidity of different
  * cities are calculated and shown in table
  */
  componentDidMount(){
    let cities = [
      { 'city': 'Dallas', 'countryCode': 'us'}, { 'city': 'Texas', 'countryCode': 'us'},
      { 'city': 'Ankara', 'countryCode': 'tr'}, { 'city': 'Dubai', 'countryCode': 'ae'},
      { 'city': 'Islamabad', 'countryCode': 'pk'}, { 'city': 'Cape Town', 'countryCode': 'za'},
      { 'city': 'London', 'countryCode': 'gb'}, { 'city': 'Zurich', 'countryCode': 'ch'},
      { 'city': 'Birmingham', 'countryCode': 'gb'}, { 'city': 'Colombo', 'countryCode': 'lk'},
      { 'city': 'Manchester', 'countryCode': 'gb'}, { 'city': 'Lahore', 'countryCode': 'pk'},
      { 'city': 'Durban', 'countryCode': 'za'}, { 'city': 'Port Elizabeth', 'countryCode': 'za'},
      { 'city': 'Welkom', 'countryCode': 'za'}, { 'city': 'Moscow', 'countryCode': 'ru'},
      { 'city': 'Geneva', 'countryCode': 'ch'}, { 'city': 'Baden', 'countryCode': 'ch'}
    ];
    let props = this.props;
    cities.forEach(function(result){
      props.fetchWeather(result.city, result.countryCode);
    });
  }

  /*
  * @params: null
  * @function: generatePDF()
  * This function creates a PDF and shows in new tab in the browser
  */
  generatePDF(){
    var doc = new pdfConverter('p','pt','c6');
    doc.setFontType("normal");
    doc.setFontSize(15);
    doc.text(15, 45, 'Average Weather 5 days Forecast');

    doc.setFontSize(3);
    var specialElementHandlers = {
    	'#skipBtn': function(element, renderer){
    		return true;
    	}
    };

    doc.fromHTML(document.body, 35, 35, {
    	'width': 2000,
    	'elementHandlers': specialElementHandlers
    });

    //doc.save("generatedpdf.pdf"); save to pdf
    doc.output('dataurlnewwindow');
  }

  render(){
    let appendData='';
    if(this.props.weather.length > 0){
      appendData = this.props.weather.map(function(cityData){
        const name = cityData.city.name;
        const temps = _.map(cityData.list.map(weather => weather.main.temp), temp => temp-273);
        const pressures = cityData.list.map(weather => weather.main.pressure);
        const humidities = cityData.list.map(weather => weather.main.humidity);

        const averageTemp = _.round(_.sum(temps)/temps.length);
        const averagePressures = _.round(_.sum(pressures)/pressures.length);
        const averageHumidities = _.round(_.sum(humidities)/humidities.length);

        return(
          <Table.Row key={ name }>
            <Table.Cell>{ name }</Table.Cell>
            <Table.Cell>{ averageTemp }</Table.Cell>
            <Table.Cell>{ averagePressures }</Table.Cell>
            <Table.Cell>{ averageHumidities }</Table.Cell>
          </Table.Row>
        );
      });
    }else{
      appendData = '';
    }
    return(
      <Divider>
        <Header as='h2'>Average 5 days weather forecast</Header>
        <Table striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>City</Table.HeaderCell>
              <Table.HeaderCell>Temperature (C)</Table.HeaderCell>
              <Table.HeaderCell>Pressure (hPa)</Table.HeaderCell>
              <Table.HeaderCell>Humidity (%)</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            { appendData }
          </Table.Body>
        </Table>
        <Button primary id='skipBtn' onClick={ this.generatePDF.bind(this) }>Generate PDF</Button>
      </Divider>
    );
  }
}

function mapStateToProps(state){
  return{
    weather: state.weather
  }
}

function mapDispatchToProps(dispatch){
  return{
    fetchWeather: (city, countryCode) => dispatch(weatherActions.fetchWeather(city, countryCode))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherList);
