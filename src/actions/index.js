const API_KEY = 'c25b946db6e1385d48b75663fd822bc5';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

//REDUX principle: ACTION can update states, only 
export function fetchWeather(city, countryCode){
  const url = `${ROOT_URL}&q=${city},${countryCode}`;
  return function(dispatch){
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      }
    })
    .then(function(response){
        return response.json();
    })
    .then(function(data){
       console.log(data);
      return dispatch({ type: FETCH_WEATHER, payload: data });
    })
    .catch(function(err){
       return dispatch({ type: 'FETCH_WEATHER_REJECTED', payload: err });
    });
  }
}
